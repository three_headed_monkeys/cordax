#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parser.py
#  
#  Copyright 2019 scimmia <scimmia@scimmia-ThinkPad-L540>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import matplotlib.pylab as plt
import json
def leggifasta(database): #legge un file fasta e lo converte in un dizionario
		f=open(database)
		uniprot=f.readlines()
		f.close()
		dizio={}
		for i in uniprot:
			#c=re.match(">(\w+)",i)  4
		
			if i[0]=='>':
					if '|' in i:
						uniprotid=i.strip('>\n').split('|')[1]
					else:
						uniprotid=i.strip('>\n').split(' ')[0]
					dizio[uniprotid]=''
			else:
				dizio[uniprotid]=dizio[uniprotid]+i.strip('\n')
		return dizio
def parse_foldx(fil='dataset/foldx_waltzdb.data'):
	seq=[]
	score=[]
	for i in open(fil).readlines():
		a=i.split()
		seq+=[a[1]]
		s=float(a[3])
		if a[0]=="Amyloid":
			score+=[1]
		else:
			score+=[0]
	return seq,score
def parse_amyload(fil='dataset/amyload/amyload.json',print_statistics=False):
	data = json.load(open(fil))['selected']['fragment']
	conc_hash={}
	controhash={}
	diz={}
	cont=0
	stats_cond={}
	le=[]
	agg_dist={}
	sources={}
	sou={}
	for i in data:
		if not i['solution']=='Measured at the concentration of 1 mM in 50 mM phosphate buffer pH 7.4, 0.05% sodiumazide.':
			continue
			pass
		nam =i['id']
		seq =i['sequence']
		agg =i['fibrilation']=='Yes'
		#if i['solution']=='Measured at the concentration of 1 mM in 50 mM phosphate buffer pH 7.4, 0.05% sodiumazide.':
		#	assert i['source']['name']=='WALTZ'
		##### conditions #####
		if not conc_hash.has_key(i['solution']):
			conc_hash[i['solution']]=cont
			stats_cond[i['solution']]=0
			controhash[cont]=i['solution']
			agg_dist[i['solution']]=[]
			cont+=1
		if i.has_key('source'):
			try:
				if not sou.has_key(i['source']['name']):
					#print i['source']['name']
					sou[i['source']['name']]=0
					
				sou[i['source']['name']]+=1
			except:
				pass
		le+=[len(seq)]
		diz[nam]={}
		diz[nam]['seq']=seq
		diz[nam]['seq1']=seq
		diz[nam]['seq2']=seq
		diz[nam]['aggr']=agg
		agg_dist[i['solution']]+=[agg]
		stats_cond[i['solution']]+=1
		diz[nam]['cond']=conc_hash[i['solution']]
	
	if print_statistics:
		a=[]
		for i in stats_cond.keys():
			#print stats_cond[i]
			a+=[(stats_cond[i],i,'\t'+str(agg_dist[i].count(1))+'\t'+str(agg_dist[i].count(0)))]
		print 'condition\tnumber\tpositives\tnegatives\t'
		for i in sorted(a,reverse=True):
			pass
			#print i
			#print i[1],'\t',i[0],'\t',i[2]
		for i in sou.keys():
			print i,'\t',sou[i]
		plt.hist(le,bins=100)
		plt.xlabel('Peptide length')
		plt.ylabel('#')
		plt.show()
	return diz
def foldex_140(fil='dataset/foldex_dg_140.data'):
	ha={}
	l=[]
	cont=0
	for i in open(fil).readlines()[1:]:#salto header
		if cont==0:
			l=i.strip().split()[1:]
			cont+=1
			continue
		a=i.strip().split()
		seq=a[0]
		c=a[1:]
		v=[]
		for k in c:
			v+=[float(k)]
		ha[seq]=v# len(v),v[-1]
		cont+=1
	return l,ha

def foldex_140_class(fil='dataset/foldex_dg_140.data'):
	ha={}
	l=[]
	cont=0
	#print fil
	for i in open(fil).readlines()[1:]:#salto header
		
		if cont==0:
			l=i.strip().split()[1:]
			cont+=1
			continue
		a=i.strip().split("\t")
		
		seq=a[0]
		c=a[1:]
		v=[]
		for k in c:
			v+=[float(k)]
		ha[seq]=v# len(v),v[-1]
		cont+=1
	return l,ha

def parse_scrumbled_140(fol='dataset/amyl33/'):
	
	pr={}
	for p in os.listdir(fol):
		

		l=[]
		
		ha={}
		cont=0
		fil=fol+p
		if p=='ApoC2_energies.txt':
			continue
		lines=open(fil).readlines()[:]
		for i in lines:
			if cont==0:
				l=i.strip().split()[1:-1]
				cont+=1
				continue
			if i.strip()=='\n':
				continue
			
			a=i.strip().split()
			#print len(a),p
			seq=a[1]
			agg=a[0]
			c=a[1:]
			v=[]
			for k in c:
				v+=[float(k)]
			ha[cont-1]={}
			#print p,cont, len(v)
			#assert len(v)==140
			ha[cont-1]['fea']=v
		
		
		pr[p.split('_')[0]]=(ha,[])
	return pr	
def parse_amyl33_140(fol='dataset/amyl33/'):
	
	pr={}
	for p in os.listdir(fol):
		

		l=[]
		
		ha={}
		cont=0
		fil=fol+p
		if p=='ApoC2_energies.txt':
			continue
		lines=open(fil).readlines()[:]
		for i in lines:
			if cont==0:
				l=i.strip().split()[1:-1]
				cont+=1
				continue
			if i.strip()=='\n':
				continue
			
			a=i.strip().split()
			#print len(a),p
			seq=a[1]
			agg=a[0]
			c=a[2:]
			v=[]
			for k in c:
				v+=[float(k)]
			ha[cont-1]={}
			#print p,cont, len(v)
			#assert len(v)==140
			ha[cont-1]['fea']=v
			
			if agg=='NA':
				ha[cont-1]['aggr']=0
			if agg=='A':
				ha[cont-1]['aggr']=1
			ha[cont-1]['seq']=seq
			cont+=1
		seqlen=len(ha.keys())
		
		lab=[0]*(seqlen+5)
		for i in range(len(ha.keys())):
			if ha[i]['aggr']==1:
				for j in range(6):
					lab[i+j]=1
		
		pr[p.split('_')[0]]=(ha,lab)
	return pr

def parse_peptides(fil="dataset/peptide_labels.csv"):

	seq=[]
	score=[]
	for i in open(fil,"r").readlines():
		a=i.strip().split("\t")
		#diz[a[0]]={}
		#diz[a[0]]["seq"]=a[0]
		seq+=[a[0]]
		if a[1]=="amyloid":
			score+=[1]
			#diz[a[0]]["aggr"]=1
		elif a[1]=="non-amyloid":
			#diz[a[0]]["aggr"]=0
			score+=[0]
		else:
			asd
	return seq, score
	
def main(args):
    print parse_peptides()

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
