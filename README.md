# CORDAX

CORDAX is an aggregation propensity predictor based on predicted packing energies.

### Installation

CORDAX is implemented in python2.7. The current implementation supports Linux and Mac operative systems.
The following libreries are required :
  - FoldX:
    - go to http://foldxsuite.crg.eu/ and register for an account
    - once logged in, download FoldX Suite 5.0 and extract the binary to the *./foldx* directory
    - when you first run it, it will create all necessary runtime files, including the "rotabase"
  - sklearn  (it can be installed with `pip install sklearn`)
  - biopython version 1.72 (it can be installed with `pip install -Iv biopython==1.72`)
  - numpy (it can be installed with `pip install numpy`)
  - matplotlib (it can be installed with `pip install matplotlib`)

### Usage

From the CORDAX folder, run python standalone.py with the following options:

  - `-h` : shows help
  - `-i` : input FASTA file
  -  `-fit` : re-trains the model and saves it
  -  `-o` : output file

### Examples

The "test" folder contains two fasta files, one with hexapeptides and one with longer fragments.
To test the tool run:
`python2 standalone.py -i test/test_proteins.fasta -o outdir`
